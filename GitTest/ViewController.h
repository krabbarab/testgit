//
//  ViewController.h
//  GitTest
//
//  Created by Peter Krabbe on 31/07/14.
//  Copyright (c) 2014 Riot Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@end

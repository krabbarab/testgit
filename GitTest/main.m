//
//  main.m
//  GitTest
//
//  Created by Peter Krabbe on 31/07/14.
//  Copyright (c) 2014 Riot Development. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
